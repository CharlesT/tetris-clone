/**
 * Module containing game logic, exposes [[`getView`]] to access the game.
 *
 * @module
 */

import * as PIXI from "pixi.js";
import { GlowFilter } from "@pixi/filter-glow";

//
// SECTION: Types
//

/**
 * All information and objects relating to a block.
 *
 * @remarks
 * Currently, the sprite is actually a graphics object, but they have
 * a similar enough API.
 */
interface Block {
  /**  Analogous to the actual sprite, saved for efficiency. */
  sprite: PIXI.Graphics;
  /**  Unit distance from left of main grid. */
  x: number;
  /**  Unit distance from top of main grid. */
  y: number;
  /**  Shape of the block. True indicates a piece, False empty space.
   * All rows and columns must contain at least one true (grid is minimal) */
  grid: Grid;
}

/**
 * Details which directions have a border (for a part of a grid)
 *
 * @remarks
 * Could do with a re-work.
 *
 * @todo simplify this. Perhaps use the `Key` enum.
 */
interface Border {
  Top: boolean;
  Bottom: boolean;
  Left: boolean;
  Right: boolean;
}

/**
 * Enum representing accepted keystrokes in the game.
 */
enum Key {
  UP = 0,
  DOWN = 1,
  LEFT = 2,
  RIGHT = 3,
  NONE = 4,
}

/**
 * Object for keeping track of values which change as the game progresses.
 *
 * @remarks
 * Does not include variables which are simply mutated over time, but ones
 * which are re-assigned frequently or immutable. This simplifies tracking
 * the state.
 */
interface GameState {
  /** Time in ms since the start of the game. Useful for timing events. */
  timeElapsed: number;
  /** Time in ms between automatic block movements. Will decrease over time. */
  fallInterval: number;
  /** The block currently falling in the game. */
  currBlock: Block;
  /** Keyboard keys currently being pressed down. Must be initialised with
   * length equal to count of Keypress enum values. */
  keys: number[];
}

/**
 * 2D array of booleans to draw shapes, where a true value means a tile
 * exists there, and false means empty space.
 * */
type Grid = boolean[][];

//
// SECTION: Constants & utility functions
//

const WIDTH = 10, // unit-less width of the play area.
  HEIGHT = 25, // unit-less height of the play area.
  STARTINTERVAL = 100, // length of time (ms) between single vertical movements.
  INPUTDELAY = 4, // length of time (ms) between single horizontal movements.
  COLOUR = 0x0000ff; // background colour.

/**
 * Generate a pseudo-random integer between `min` and `max`.
 *
 * @remarks
 * Due to the nature of `Math.random`, the maximum value will never be
 * generated, but the minimum might.
 *
 * @param min - Minimum output value
 * @param max - Maximum output value
 * @returns random value between `min` and `max`
 */
const randint = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
 * Change the x-value of the input `block` to a new value.
 *
 * @remarks
 * The `block` object's x attribute is changed as expected, but the position
 * value in the sprite must also be changed so that the sprite's location on
 * the screen is updated too. This is why `tileSize` is a required input.
 *
 * @param block - object to update the x value of.
 * @param tileSize - physical dimensions of each tile making up the block
 * @param units - value to update x to.
 */
const setX = (block: Block, tileSize: number, units: number): void => {
  block.x = units;
  block.sprite.x = tileSize * units;
};

/**
 * Change the y-value of the input `block` to a new value.
 *
 * @remarks
 * The `block` object's y attribute is changed as expected, but the position
 * value in the sprite must also be changed so that the sprite's location on
 * the screen is updated too. This is why `tileSize` is a required input.
 *
 * @param block - object to update the y value of.
 * @param tileSize - physical dimensions of each tile making up the block
 * @param units - value to update y to.
 */
const setY = (block: Block, cellsize: number, units: number): void => {
  block.y = units;
  block.sprite.y = cellsize * units;
};

/**
 * Translate string `keyname` into an enum.
 *
 * @remarks
 * Only recognises Arrow keys. All other inputs result in Key.NONE.
 *
 * @param keyname - string received from a keyboard event.
 * @returns member of the `Key` enum.
 */
const makeKey = (keyname: string): Key => {
  switch (keyname) {
    case "ArrowUp":
      return Key.UP;
    case "ArrowDown":
      return Key.DOWN;
    case "ArrowLeft":
      return Key.LEFT;
    case "ArrowRight":
      return Key.RIGHT;
    default:
      return Key.NONE;
  }
};

/**
 * Array of all block shapes to be used.
 *
 * @remarks
 * Currently just all possible shapes using exactly 4 square blocks. They are
 * represented using 2D boolean arrays, where a true value represents a block
 * and a false value represents empty space.
 *
 * @returns Array of Grids representing block shapes.
 */
const blockShapes: Grid[] = [
  [[true], [true], [true], [true]],
  [
    [false, true],
    [false, true],
    [true, true],
  ],

  [
    [true, false],
    [true, false],
    [true, true],
  ],

  [
    [true, false],
    [true, true],
    [false, true],
  ],

  [
    [false, true],
    [true, true],
    [true, false],
  ],

  [
    [false, true, false],
    [true, true, true],
  ],

  [
    [true, true],
    [true, true],
  ],
];

//
// SECTION: Pixijs integration
//

/**
 * Generate the PIXI application and return its view
 *
 * @remarks
 * As this is the only exported function, it ties together the rest of the
 * code from this module.
 *
 * @param res - PIXI app resolution value. Default to 1.
 * @param tileSize - number of pixels per tile. Default to 50.
 * @returns Canvas element containing the entire game, scaled to window.
 */
export const getView = (
  res: number = 1,
  tileSize: number = 50
): HTMLCanvasElement => {
  // Create screen
  const app = new PIXI.Application({
    height: (HEIGHT + 1) * tileSize,
    width: 2 * WIDTH * tileSize,
    resolution: res,
    backgroundAlpha: 0,
    antialias: true,
    // resizeTo: window, // Doesn't really work for some reason.
  });

  // Scale screen size to browser on resize
  import("./scaleToWindow").then((f) => {
    f.default(app.view, "black");
    window.addEventListener("resize", () => {
      f.default(app.view, "black");
    });
  });

  const tileGlow = new GlowFilter({
    distance: 20,
    outerStrength: 9,
    knockout: true,
    innerStrength: 1,
    color: 0x04d9ff,
    quality: 0.2,
  });
  // Get the tetronimo sprites ready!
  const tiles = blockShapes.map((grid) => makeBlock(grid, tileSize, tileGlow));

  const grid = createGrid(WIDTH, HEIGHT);
  grid[HEIGHT - 1].forEach((_, i) => (grid[HEIGHT - 1][i] = true));
  const big = makeBlock(grid, tileSize, tileGlow);
  big.sprite.filters = [tileGlow];
  big.sprite.position.x = 0;
  big.sprite.position.y = 0;

  app.stage.addChild(big.sprite);

  const state: GameState = {
    timeElapsed: 0,
    fallInterval: STARTINTERVAL,
    currBlock: newBlock(app, tiles, tileSize),
    keys: Array.from(Array(Key.NONE)).map(() => -1),
  };

  window.addEventListener("keydown", ({ key }) => {
    const keyname = makeKey(key);
    if (state.keys[keyname] < 0) state.keys[keyname] = INPUTDELAY;
  });

  window.addEventListener("keyup", ({ key }) => {
    state.keys[makeKey(key)] = -1;
  });

  app.ticker.add((dt) => gameLoop(app, tiles, big, state, tileSize, dt));

  return app.view;
};

/**
 * Main code, executed each frame, controlling game operation.
 *
 * @remarks
 * Actions include:
 * - spawning new blocks when necessary
 * - moving blocks according to gravity & user input
 * - collision detection
 * - game status updates
 *
 * @param app - full access to game is required to add and remove blocks, or pause.
 * @param tiles - array of all pre-generated blocks, including their sprites.
 * @param base - block representing the growing bottom block & space above.
 * @param state - object keeping track of time and other state.
 * @param tileSize - number of pixels for the width of each square.
 * @param dtime - delta-time, the time in ms since the loop was last run.
 */
const gameLoop = (
  app: PIXI.Application,
  tiles: Block[],
  base: Block,
  state: GameState,
  tileSize: number,
  dtime: number
) => {
  state.timeElapsed += dtime;
  const {
    timeElapsed: totalT,
    fallInterval: interval,
    currBlock: block,
    keys: keys,
  } = state;

  // Take user input
  keys.forEach((_, key) => {
    // If the key is currently being pressed, increment the waiting time.
    if (keys[key] >= 0) keys[key] += dtime;

    // If this is the first press, or enough time has passed, make a move.
    if (keys[key] > INPUTDELAY) {
      keys[key] = 0;
      switch (key) {
        case Key.LEFT:
          if (freeToMove([base], block, Key.LEFT))
            setX(block, tileSize, block.x - 1);
          break;
        case Key.RIGHT:
          if (freeToMove([base], block, Key.RIGHT))
            setX(block, tileSize, block.x + 1);
          break;
        case Key.UP:
          // Rotate the block
          break;
        case Key.DOWN:
          if (updateGrid(base, block, tileSize)) app.render();
          break;
        default:
          break;
      }
    }
  });

  // Move or add to the base.
  if (totalT % interval < dtime) {
    // Time for the next movement.
    const timeForNewBlock = updateGrid(base, block, tileSize);

    if (timeForNewBlock) {
      app.stage.removeChild(state.currBlock.sprite);
      state.currBlock = newBlock(app, tiles, tileSize);
      app.render();
    }
  }
};

//
// SECTION: Grid & block manipulation
//

/**
 * Create a new 2d array of booleans
 *
 * @remarks
 * Used to describe the shape of blocks, see [[`newBlock`]].
 *
 * @param width - width of the new grid in units.
 * @returns 2D array of false values, where each inner array represents a row.
 */
const createGrid = (width: number, height: number): Grid => {
  // width x height grid filled with BlockT.Empty.
  return Array.from(Array(height)).map(() =>
    Array.from(Array(width)).map(() => false)
  );
};

/**
 * Utility function to check if an element of the grid satisfies a given function.
 *
 * @remarks
 * Particularly useful for bounds checking. Uses Array.some(), so the moment
 * true is returned by `fn`, the operation terminates.
 *
 * @param grid - the 2D array to search through
 * @param fn - condition function taking the value, x, and y of each cell in grid.
 * @returns boolean representing whether the condition was satisfied.
 */
const searchGrid = (
  grid: Grid,
  fn: (b: boolean, dx: number, dy: number) => boolean
): boolean => {
  return grid.some((row, dy) => row.some((isBlock, dx) => fn(isBlock, dx, dy)));
};

/**
 * Update time by one moment in the game.
 *
 * @remarks
 * This involves moving any falling blocks downwards by one unit, and merging
 * with the base if contact is made.
 *
 * @param base - big block at the bottom, waiting for collision.
 * @param block - current falling block.
 * @param tileSize - size in pixels of each tile comprising a block.
 * @returns boolean representing whether a collision & merge has occurred.
 */
const updateGrid = (base: Block, block: Block, tileSize: number): boolean => {
  const isCurrentlyColliding = searchGrid(block.grid, (isBlock, dx, dy) => {
    return isBlock && base.grid[block.y + dy + 1][block.x + dx];
  });

  if (isCurrentlyColliding) {
    addBlockToBase(base, block, tileSize);
    return true;
  } else {
    // Just move the block down.
    setY(block, tileSize, block.y + 1);
    return false;
  }
};

/**
 * Pick the next block to fall and add it to the game.
 *
 * @remarks
 * Randomly selects a block from the `tiles` selection, then adds it the stage.
 *
 * @param app - full application object, to add the block to.
 * @param blocks - the selection of possible blocks.
 * @param app - full application object, to add the block to.
 * @returns the selected block, for manipulation in `gameLoop`.
 */
const newBlock = (
  app: PIXI.Application,
  blocks: Block[],
  tileSize: number
): Block => {
  const nextBlock = blocks[randint(0, blocks.length - 1)];

  setY(nextBlock, tileSize, 0);
  setX(nextBlock, tileSize, WIDTH / 2);

  app.stage.addChild(nextBlock.sprite);
  return nextBlock;
};

/**
 * Merges a block with the base.
 *
 * @remarks
 * Should only be called on collision with a falling block and the base block.
 * Performs a merge operation which removes block from the stage and updates
 * the sprite of `base`.
 *
 * @param base - base block at the bottom of the stage, to be extended.
 * @param block - the block to merge with the base.
 * @param block - size in pixels of each tile in a block.
 */
const addBlockToBase = (base: Block, block: Block, tileSize: number): void => {
  searchGrid(block.grid, (isBlock, dx, dy) => {
    if (isBlock) {
      // Update base
      base.grid[block.y + dy][block.x + dx] = true;
    }
    return false; // No need to exit search early, so never return true.
  });
  drawShape(base, tileSize, COLOUR, base.sprite.filters);
};

/**
 * Check if it's possible to move in the specified direction, without a
 * collision.
 *
 * @remarks
 * Very simple collision detection using the grid-based nature of the game.
 *
 * @param obstacles - blocks to be careful of colliding with.
 * @param block - block trying to move.
 * @param direction - intended direction to move in.
 * @returns boolean which if true, indicates that the `block` is free to move.
 */
const freeToMove = (
  obstacles: Block[],
  block: Block,
  direction: Key
): boolean => {
  // Negation is necessary, as a true value would indicate that at some point
  // an obstacle was encountered and so the arr.some method terminated early.
  return !obstacles.some((otherBlock) => {
    return searchGrid(block.grid, (isBlock, dx, dy) => {
      if (isBlock)
        // Only check for collisions for parts of the block.
        switch (direction) {
          case Key.LEFT:
            return otherBlock.grid[block.y + dy][block.x + dx - 1];
          case Key.RIGHT:
            return otherBlock.grid[block.y + dy][block.x + dx + 1];
          case Key.UP:
            return otherBlock.grid[block.y + dy - 1][block.x + dx];
          case Key.DOWN:
            return otherBlock.grid[block.y + dy + 1][block.x + dx];
          default:
            return false; // Playing it safe. Weird input means no movement.
        }
      else return false;
    });
  });
};

//
// SECTION: Sprite drawing
//

/**
 * Create a new block, with shape determined by `grid`.
 *
 * @remarks
 * Block is given a (0,0) position.
 *
 * @param grid - shape of the block, described using a 2d boolean array.
 * @param tileSize - size in pixels of the tiles making up the block.
 * @param filter - a pixijs object which decorates the sprite.
 * @returns the new block.
 */
const makeBlock = (
  grid: Grid,
  tileSize: number,
  filter: PIXI.Filter
): Block => {
  const newBlock = {
    sprite: new PIXI.Graphics(),
    grid: grid,
    x: 0,
    y: 0,
  };

  // Set aesthetics

  drawShape(newBlock, tileSize, COLOUR, [filter]);

  return newBlock;
};

/**
 * Re-draw a block's sprite to match its grid.
 *
 * @remarks
 * Used both for new blocks and updating existing ones.
 *
 * @param block - the block which will have its sprite updated.
 * @param tileSize - size in pixels of the tiles making up the block.
 * @param colour - colour to draw the sprite in.
 * @param filters - pixijs objects which adjust the look of the sprite.
 */
const drawShape = (
  block: Block,
  tileSize: number,
  colour: number,
  filters: PIXI.Filter[]
): void => {
  const { grid: grid, sprite: outline } = block;

  outline.clear();

  outline.lineStyle(1, colour, 10);
  outline.filters = filters;

  // Iterate through the tile map and generate border instructions.
  const borders: Border[][] = grid.map((row, i) =>
    row.map((isBlock, j) => findBorder(grid, isBlock, i, j))
  );

  // Use the instructions to draw the sprite.
  let x, y;
  borders.forEach((row, i) =>
    row.forEach((border, j) => {
      (x = j * tileSize), (y = i * tileSize);
      outline.moveTo(x, y);
      if (border.Top) outline.lineTo(x + tileSize, y);
      outline.moveTo(x, y);
      if (border.Left) outline.lineTo(x, y + tileSize);
      outline.moveTo(x + tileSize, y + tileSize);
      if (border.Bottom) outline.lineTo(x, y + tileSize);
      outline.moveTo(x + tileSize, y + tileSize);
      if (border.Right) outline.lineTo(x + tileSize, y);
    })
  );

  outline.finishPoly();
};

/**
 * For a given part of a grid, determine if it has an edge(s)
 * or not, and where they are.
 *
 * @remarks
 * Fairly rudimentary, just checks for neighbouring false values
 * if it is true.
 *
 * @param arr - The grid currently being accessed.
 * @param isBlock - aptly named. True if arr[i,j] is true.
 * @param i - current row of arr being accessed.
 * @param j - current column of arr being accessed.
 * @returns border object describing which of the 4 cardinal directions are
 * currenly borders at (i,j).
 */
const findBorder = (
  arr: Grid,
  isBlock: boolean,
  i: number,
  j: number
): Border => {
  // Check bounds
  let up = i > 0,
    down = i < arr.length - 1,
    left = j > 0,
    right = j < arr[i].length - 1;

  // If bounds check was ok, check for adjacent blocks.
  if (up) up = arr[i - 1][j];
  if (down) down = arr[i + 1][j];
  if (left) left = arr[i][j - 1];
  if (right) right = arr[i][j + 1];

  return {
    Top: isBlock && !up,
    Bottom: isBlock && !down,
    Left: isBlock && !left,
    Right: isBlock && !right,
  };
};
