import { getView } from "./tetris";

const component = async () => {
  const element = document.createElement("div");

  const screen = await getView(window.devicePixelRatio, 50);
  element.appendChild(screen);

  return element;
};

component()
  .then((c) => {
    document.body.appendChild(c);
  })
  .catch((error) => "Whoops");
